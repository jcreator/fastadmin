define([], function () {
    //修改上传的接口调用
require(['upload', '../addons/cos/js/spark'], function (Upload, SparkMD5) {
    var _onFileAdded = Upload.events.onFileAdded;
    var _onUploadResponse = Upload.events.onUploadResponse;
    var _process = function (up, file) {
        (function (up, file) {
            var blob = file.getNative();
            var loadedBytes = file.loaded;
            var chunkSize = 2097152;
            var chunkBlob = blob.slice(loadedBytes, loadedBytes + chunkSize);
            var reader = new FileReader();
            reader.addEventListener('loadend', function (e) {
                var spark = new SparkMD5.ArrayBuffer();
                spark.append(e.target.result);
                var md5 = spark.end();
                Fast.api.ajax({
                    url: "/addons/cos/index/params",
                    data: {method: 'POST', md5: md5, name: file.name, type: file.type, size: file.size},
                }, function (data) {
                    file.md5 = md5;
                    file.status = 1;
                    file.key = data.key;
                    file.filename = data.filename;
                    file.token = data.token;
                    file.signature = data.signature;
                    file.notifysignature = data.notifysignature;
                    up.start();
                    return false;
                });
                return;
            });
            reader.readAsArrayBuffer(chunkBlob);
        })(up, file);
    };
    Upload.events.onFileAdded = function (up, files) {
        return _onFileAdded.call(this, up, files);
    };
    Upload.events.onBeforeUpload = function (up, file) {
        if (typeof file.md5 === 'undefined') {
            up.stop();
            _process(up, file);
        } else {
            up.settings.headers = up.settings.headers || {};
            up.settings.multipart_params.key = file.key;
            up.settings.multipart_params.Signature = file.signature;
            up.settings.multipart_params.success_action_status = 200;
            up.settings.multipart_params['Content-Disposition'] = 'inline; filename=' + file.filename;
            up.settings.multipart_params['Content-Type'] = file.type;
            up.settings.multipart_params['x-cos-security-token'] = file.token;
            up.settings.send_file_name = false;
        }
    };
    Upload.events.onUploadResponse = function (response, info, up, file) {
        try {
            var ret = {};
            if (info.status === 200) {
                var url = '/' + file.key;
                Fast.api.ajax({
                    url: "/addons/cos/index/notify",
                    data: {
                        method: 'POST',
                        name: file.name,
                        url: url,
                        md5: file.md5,
                        size: file.size,
                        type: file.type,
                        signature: file.signature,
                        token: file.token,
                        notifysignature: file.notifysignature
                    }
                }, function () {
                    return false;
                });
                ret.code = 1;
                ret.data = {
                    url: url
                };
            } else {
                ret.code = 0;
                ret.msg = info.response;
            }
            return _onUploadResponse.call(this, JSON.stringify(ret));

        } catch (e) {
        }
        return _onUploadResponse.call(this, response);

    };
});
require.config({
    paths: {
        'webix': '../libs/webix/codebase/webix',
        'filemanager': '../libs/webix/codebase/filemanager',
    },
    shim: {
        "filemanager": {
            deps: ["webix"],
            exports: "filemanager"
        }
    }
});
require.config({
    paths: {
        'slider': '../addons/recruit/js/bootstrap-slider.min'
    },
    shim:{
        'slider': ['css!../addons/recruit/css/bootstrap-slider.min.css'],
    }
});
require(['form', 'upload'], function (Form, Upload) {
    var _bindevent = Form.events.bindevent;
    Form.events.bindevent = function (form) {
        _bindevent.apply(this, [form]);
        try {
            if ($(".slider", form).size() > 0) {
                    require(['slider'], function () {
                     //   console.log($(document).find(".slider-horizontal").css("margin-left",'%5'));
                    //$(this).slider();
                    /*
                        $(this).slider({  
                            formatter: function (value) {  
                                return 'Current value: ' + value;  
                            }  
                        }).on('slide', function (slideEvt) {  
                            console.info(slideEvt);  
                        }).on('change', function (e) {  
                            console.info(e.value.oldValue + '--' + e.value.newValue);  
                        });  
                    */
                    });
                }
        } catch (e) {

        }

    };
});

require(['../addons/wangeditor/wangEditor', 'upload'], function (Editor, Upload) {
    var editor;
    $(".editor").each(function () {
        $(this).hide();
        var that = this;
        var id = $(this).attr("id");
        $("<div />").attr("id", id).insertAfter(this);
        editor = new Editor('#' + id);
        editor.customConfig.customUploadImg = function (files, insert) {
            for (var i = 0; i < files.length; i++) {
                Upload.api.send(files[i], function (data) {
                    var url = Fast.api.cdnurl(data.url);
                    insert(url);
                });
            }
        };
        editor.customConfig.onchange = function (html) {
            $(that).val(html);
        };
        editor.customConfig.menus = [
            'head',  // 标题
            'bold',  // 粗体
            'fontSize',  // 字号
            'fontName',  // 字体
            'italic',  // 斜体
            'underline',  // 下划线
            'strikeThrough',  // 删除线
            'foreColor',  // 文字颜色
            'backColor',  // 背景颜色
            'link',  // 插入链接
            'list',  // 列表
            'justify',  // 对齐方式
            'quote',  // 引用
            'emoticon',  // 表情
            'image',  // 插入图片
            'select',  // 插入图片
            'table',  // 表格
            'video',  // 插入视频
            'code',  // 插入代码
            'undo',  // 撤销
            'redo'  // 重复
        ];
        editor.customConfig.zIndex = 100;
        editor.create();
        editor.txt.html($(this).val());
        $(this).data("wangeditor", editor);
    });

});

});