<?php

namespace addons\third\model;

use think\Model;
use think\Session;

class ThirdUser extends \think\Model
{

    // 表名
    protected $name = 'user_third';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';


}
