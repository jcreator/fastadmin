<?php
/**
 * Created by Rock.
 * User: Administrator
 * Date: 2019-3-14
 * Time: 上午 8:33
 */
namespace addons\third\library;

use addons\third\model\ThirdUser;
use app\admin\model\User;
use think\Db;

class Service
{
    /**
     * 根据openid，查找对应的用户id
     *
     * @author Rock
     * @param $platform
     * @param $data
     * @param $extend
     * @return Boolean|String $id   用户id or FALSE
     * @throws \think\Exception
     */
    public static function connect($platform, $data, $extend)
    {
        if ($platform !== 'wxapp') return FALSE;
        if (isset($data['openid'])){
            $ret = ThirdUser::get(['openid'=> $data['openid']]);
            if (is_null($ret)){
                Db::startTrans();
                try {
                    $user_id = Db::name('user')->insertGetId(['nickname'=>$extend['nickname'], 'gender'=>$extend['gender'], 'avatar'=>$extend['avatar'], 'status'=>'normal']);
                    Db::name('user_third')->insert(['openid'=>$data['openid'], 'user_id'=>$user_id, 'platform'=>$platform]);
                    $ret = ['openid'=>$data['openid'], 'user_id'=>$user_id];
                    Db::commit();
                } catch (\Exception $e) {
                    Db::rollback();
                    return false;
                }
            }
            $ret1 = User::get(['id'=> $ret['user_id']])->toArray();
            if (!is_array($ret1)){
                $user_id = Db::name('user')->insertGetId(['nickname'=>$extend['nickname'], 'gender'=>$extend['gender'], 'avatar'=>$extend['avatar'], 'status'=>'normal']);
            }
            return $ret['user_id'];
        }

        return FALSE;


    }
}