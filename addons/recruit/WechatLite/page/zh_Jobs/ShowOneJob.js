var app = getApp();

Page({

  data: {
    id:null,
    comjob:[],
    PicAllData: [],
  },
  onShow: function () {
    //post 拖取数据
    var that = this;
    app.request('/Companyjob/get_c_job', { id: that.data.id }, function (data, ret) {
      console.log(data);
      data.recruitcompany.cimage = app.Domain_Public + data.recruitcompany.cimage;
      if (data.content != '') {
        data.content = data.content.replace(/[\n\r]/g, '<br>');
        data.content = app.towxml.toJson(data.content.replace(/: /g, ':'), 'html', that);
        //data.content = app.towxml.toJson(data.content, 'html', that);
        that.traverse(data.content);
      }
      if (data.recruitcompany.content != '') {
        data.recruitcompany.content = data.recruitcompany.content.replace(/[\n\r]/g, '<br>');
        data.recruitcompany.content = app.towxml.toJson(data.recruitcompany.content.replace(/: /g, ':'), 'html', that);
        that.traverse(data.recruitcompany.content);
      }
      let tmparrpic = [];
      if (data.recruitcompany.cimages != "") {
        data.recruitcompany.cimages.split(",").forEach(function (item) {
          item = app.Domain_Public + item;
          tmparrpic.push(item);
        });
      }
      that.setData({
        comjob: data,
        PicAllData: tmparrpic
      })

    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  onLoad: function (options) {
    var that = this;
    console.log(options);
    if (options.id != undefined) {
      that.setData({
        id: options.id
      })
    }
    if (options.scene != undefined) {
      var scene = decodeURIComponent(options.scene);
      that.setData({
        id: scene
      })
    }
    that.setData({
      QrcodeImg: (app.Isdebug ? app.QrPngUrl : app.apiUrl) + "Usewechat/get_Job_QrPng?id=" + that.data.id,
    }) 
  },
  pic_click: function (e) {
    var that = this;
    wx.previewImage({
      current: e.currentTarget.dataset.src,
      urls: that.data.PicAllData,
      success: function (res) {
        console.log(res);
      },
      fail: function () {
        console.log('fail')
      }
    });
  },
  traverse: function (obj) {
    for (var a in obj) {
      if (typeof (obj[a]) == "object") {
        this.traverse(obj[a]); //递归遍历  
      } else {
        if (a == 'src' && obj[a].indexOf("/uploads/") == 0) {
          //console.log(a + "=" + obj[a]); 
          obj[a] = app.Domain_Public + obj[a];
        }
      }
    }
  },
  telmake: function (options) {
    var that = this;
    wx.makePhoneCall({
      phoneNumber: that.data.comjob.recruitcompany.tel
    })
  },
  onShareAppMessage: function () {
    var that = this;
    return {
      title: '职位详情：' + that.data.comjob.name,
      path: '/page/zh_Jobs/ShowOneJob?id=' + that.data.comjob.Id
    }
  }
})