<?php

namespace addons\recruit\controller;

/**
 * 我的
 */
class My extends Base
{

    protected $noNeedLogin = ['aboutus'];

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 提交报名
     */
    public function add_baoming(){
        $row = $this->request->post();
        //增加登录人的信息
        $row['user_id'] = $this->auth->id;

        $Jobfair = new \app\admin\model\Jobfair;
        $Jobfair->user_id     = $row['user_id'];
        $Jobfair->block_id    = $row['block_id'];
        $Jobfair->block_title = $row['block_title'];

        $Jobfair->tname    = $row['tname'];
        $Jobfair->ttel     = $row['ttel'];
        
        $Jobfair->save();

        $this->success('', $row);
    }
}
