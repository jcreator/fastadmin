<?php

namespace app\reply\controller\wxapp;

use app\common\controller\Api;
use app\common\library\Auth;
use think\Config;
use think\Lang;

class Base extends Api
{

    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];
    protected $current_entity_id = ''; // 当前实体ID
    //设置返回的会员字段
    protected $allowFields = ['id', 'username', 'nickname', 'realname', 'mobile', 'avatar', 'score', 'certification', 'bind_service_id'];
    protected $reply_method = ['notice'=> '通知', 'invite'=> '邀请', 'enlist'=> '报名'];

    public function _initialize()
    {
        parent::_initialize();

        Config::set('default_return_type', 'json');
        Auth::instance()->setAllowFields($this->allowFields);

        //这里手动载入语言包
        Lang::load(APP_PATH . '/reply/lang/zh-cn.php');
        Lang::load(APP_PATH . '/index/lang/zh-cn/user.php');

        //根据传入场景值，确定当前实体Id-- rock
        if ($this->request->post('entity_id')) {
            $this->current_entity_id = $this->request->post('entity_id');
        }
    }
}
