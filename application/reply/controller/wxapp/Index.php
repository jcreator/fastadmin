<?php

namespace app\reply\controller\wxapp;

use app\reply\model\Channel;
use app\reply\model\Message as MessageModel;
use app\reply\model\Comment;
use app\reply\model\Modelx;

/**
 * 消息
 */
class Index extends Base
{

    protected $noNeedLogin = ['*'];

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 读取消息列表
     * 传递username、token、场景值
     */
    public function index()
    {
        // 1 顶端栏目
        $channelList = Channel::getAppNav($this->current_entity_id);

        /*
        // 2 轮播图
        $bannerList = [];
        $list = Block::getBlockList(['name' => 'focus', 'row' => 5]);
        foreach ($list as $index => $item) {
            $bannerList[] = ['image' => cdnurl($item['image'], true), 'url' => '/', 'title' => $item['title']];
        }
        */
        // 2 栏目列表
        //TODO::栏目列表

        // 3 消息列表（均在主表）
        // 左侧图1：团队图片/个人微信图片（image字段）
        // 文本区1：发布团队（发布人）、发布时间（本年度：5/1，往年：18/5/1）
        // 文本区2：摘要
        // 右侧图：（有则显示,并隐藏左侧图）消息发起人上传的图片：最多1个链接
        // 备注区：1置顶(及过期时间)，2所属栏目（有栏目则为官方，最懂7个字）、3回复方式：通知（收到即可）/邀请/报名、4浏览数、5评论数、6点赞数（1-3均为标签显示，4-6均为图标+数字）
        // 下侧图：消息发起人上传的图片：最多3个链接
        // 其他信息：截止日期

        /*
        $tag['field'] =
            'id, image,
            is_group, user_id, nickname, group_id, group_name, publishtime,
            description,
            upload_pics,
            is_top, expiretime_top, channel_id, reply_method, default_reply_message, views, comments, likes,
            expiretime_reply';
        */
        $messageList = MessageModel::getArchivesList(['cache'=>false, 'entity_id'=>$this->current_entity_id]);
        $data = [
            'channelList'  => $channelList,
            'archivesList' => $messageList,
            'replyMethod'  => $this->reply_method
        ];
        $this->success('', $data);
    }

    public function detail()
    {
        $action = $this->request->post("action");
        if ($action && $this->request->isPost()) {
            return $this->$action();
        }
        $diyname = $this->request->param('diyname');
        if ($diyname && !is_numeric($diyname)) {
            $archives = ArchivesModel::getByDiyname($diyname);
        } else {
            $id = $diyname ? $diyname : $this->request->request('id', '');
            $archives = ArchivesModel::get($id);
        }
        if (!$archives || ($archives['user_id'] != $this->auth->id && $archives['status'] != 'normal') || $archives['deletetime']) {
            $this->error(__('No specified article found'));
        }
        $channel = Channel::get($archives['channel_id']);
        if (!$channel) {
            $this->error(__('No specified channel found'));
        }
        $model = Modelx::get($channel['model_id']);
        if (!$model) {
            $this->error(__('No specified model found'));
        }
        $archives->setInc("views", 1);
        $addon = db($model['table'])->where('id', $archives['id'])->find();
        if ($addon) {
            if ($model->fields) {
                $fieldsContentList = $model->getFieldsContentList($model->id);
                //附加列表字段
                array_walk($fieldsContentList, function ($content, $field) use (&$addon) {
                    $addon[$field . '_text'] = isset($content[$addon[$field]]) ? $content[$addon[$field]] : $addon[$field];
                });
            }
            $archives->setData($addon);
        } else {
            $this->error(__('No specified article addon found'));
        }
        $archives = array_merge($archives->toArray(), $addon);

        $commentList = Comment::getCommentList(['aid' => $archives['id']]);

        $this->success('', ['archivesInfo' => $archives, 'channelInfo' => $channel, 'commentList' => $commentList->getCollection()]);
    }

    /**
     * 赞与踩
     */
    public function vote()
    {
        $id = (int)$this->request->post("id");
        $type = trim($this->request->post("type", ""));
        if (!$id || !$type) {
            $this->error(__('Operation failed'));
        }
        $archives = ArchivesModel::get($id);
        if (!$archives || ($archives['user_id'] != $this->auth->id && $archives['status'] != 'normal') || $archives['deletetime']) {
            $this->error(__('No specified article found'));
        }
        $archives->where('id', $id)->setInc($type === 'like' ? 'likes' : 'dislikes', 1);
        $archives = ArchivesModel::get($id);
        $this->success(__('Operation completed'), ['likes' => $archives->likes, 'dislikes' => $archives->dislikes, 'likeratio' => $archives->likeratio]);
    }

}
