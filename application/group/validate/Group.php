<?php
namespace app\group\validate;

use think\Validate;

class Group extends Validate
{
    protected $rule = [
        'id|群组ID'           => 'require|integer',
        'name|群组名称'        => 'require|chsDash|length:4,40',
        'avatar|群组封面图'    => 'require|url',
        'owner_id|创建者ID'    => 'require|integer',
        'config|配置'         => 'require'
    ];

    protected $message = [
        'id.require'        =>  '群组ID不能为空',
        'id.integer'        =>  '群组ID必须为整数',
        'name.require'      =>  '群组名称不能为空',
        'name.chsDash'      =>  '群组名称只能为汉字,字母,数字,下划线,破折号',
        'name.length'       =>  '群组名称长度必须在 4至40个字符之间',
        'avatar.require'    =>  '群组封面图不能为空',
        'avatar.url'        =>  '群组封面图地址必须为合法的URL地址',
        'owner_id.require'  =>  '创建者ID不能为空',
        'owner_id.integer'  =>  '创建者ID必须为整数',
        'config.require'    =>  '配置信息不能为空'
    ];

    protected $scene = [
        'add'       =>  ['name', 'owner_id'],
        'edit'      =>  ['id','name','avatar','owner_id'],
        'config'    =>  ['id','config']
    ];
}