<?php
namespace app\group\validate;

use think\Validate;

class GroupTag extends Validate
{
    protected $rule = [
        'id|标签ID'           => 'require|integer',
        'name|标签名称'        => 'require|chsDash|length:2,50',
        'entity_id|实体ID'    => 'integer',
        'order|排序'          => 'integer',
        'type|类型'           => 'integer',
        'status|状态'         => 'integer'
    ];

    protected $message = [
        'id.require'        =>  '标签ID不能为空',
        'id.integer'        =>  '标签ID必须为整数',
        'name.require'      =>  '标签名称不能为空',
        'name.chsDash'      =>  '标签名称只能为汉字,字母,数字,下划线,破折号',
        'name.length'       =>  '标签名称长度必须在 2至50个字符之间',
        'entity_id.integer' =>  '实体ID必须为整数',
        'order.integer'     =>  '排序数据必须为整数',
        'type.integer'      =>  '类型数据必须为整数',
        'status.integer'    =>  '状态数据必须为整数',
    ];

    protected $scene = [
        'add'       =>  ['name', 'entity_id', 'type'],
        'edit'      =>  ['id','name','avatar','owner_id'],
        'config'    =>  ['id','config']
    ];
}