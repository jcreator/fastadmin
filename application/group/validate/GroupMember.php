<?php
namespace app\group\validate;

use think\Validate;

class GroupMember extends Validate
{
    protected $rule = [
        'id|群组成员ID'         => 'require|integer',
        'group_id|群组ID'      => 'require|integer',
        'user_id|用户ID'       => 'require|integer',
        'username|用户名'      => 'length:4,50|alphaDash',
        'nickname|用户别名'     => 'length:4,50|chsDash',
        'realname|真实姓名'     => 'length:2,50|chsDash',
        'avatar|用户头像'       => 'url'
    ];

    protected $message = [
        'id.require'        =>  '群组成员ID不能为空',
        'id.integer'        =>  '群组ID必须为整数',
        'group_id.require'  =>  '群组ID不能为空',
        'group_id.integer'  =>  '群组ID必须为整数',
        'user_id.require'   =>  '用户ID不能为空',
        'user_id.integer'   =>  '用户ID必须为整数',
        'username.length'   =>  '用户名长度必须在 4至50个字符之间',
        'username.alphaDash'  =>  '用户名只能为字母,数字,下划线,破折号',
        'nickname.length'   =>  '用户别名长度必须在 4至50个字符之间',
        'nickname.chsDash'  =>  '用户别名只能为汉字,字母,数字,下划线,破折号',
        'realname.length'   =>  '真实姓名长度必须在2至50个字符之间',
        'realname.chsDash'  =>  '真实姓名只能为汉字,字母,数字,下划线,破折号',
        'avatar.url'        =>  '用户头像地址必须为合法的URL地址',
    ];

    protected $scene = [
        'add_admin'             =>  ['group_id', 'user_id', 'username', 'nickname', 'realname', 'avatar'],
        'group_member_import'      =>  ['group_id', 'username', 'realname', 'email', 'mobile'],
        'group_member_edit'                  =>  ['username', 'nickname', 'realname', 'avatar', 'email', 'mobile']
    ];
}