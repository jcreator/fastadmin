<?php
namespace app\group\controller;

/**
 * Class Tag
 * @author Rock
 * @package app\group\controller
 * 群组标签操作
 */


use think\Loader;
use think\Db;
use app\group\model\Group;
use app\group\model\GroupMember;
use app\group\model\GroupMemberImport;
use app\group\model\GroupAdmin;
use app\group\model\GroupTag;


class Tag extends Base {

    protected $noNeedLogin = [];
    protected $token = '';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function add()
    {

        // 接收数据
        $name = $this->request->post('name');
        if ($name === null) $this->error(__('param is empty'),[],2400);
        $data['name'] = $name;

        $data['user_id'] = $this->auth->id;

        $type = $this->request->post('type');
        if ($type !== null) $data['type'] = $type;

        $entity_id = $this->request->post('entity_id');
        if ($entity_id !== null) $data['entity_id'] = $entity_id;

        // 检测数据
        $validate = Loader::validate('GroupTag');
        if (!$validate->scene('add')->check($data)) $this->error($validate->getError());

        $tag_model = new GroupTag();

        // 数据查重
        if ($tag_model->where($data)->find()) $this->error(__('Duplicate data'),[],2401);

        $group_ids = $this->request->post('group_ids');
        if ($group_ids !== null) $data['group_ids'] = $group_ids;

        $result = $tag_model->allowField(true)->save($data);

        if ($result) $this->success('',$data);
        else $this->error();

    }


}