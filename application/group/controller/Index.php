<?php
namespace app\group\controller;

/**
 * Class Index
 * @author Rock
 * @package app\group\controller
 * 群组操作
 */

use think\Loader;
use app\group\model\Group;
use app\group\model\GroupMember;

class Index extends Base {

    protected $noNeedLogin = [];
    protected $token = '';

    /**
     * 创建群组
     * @param name String 群组名称
     * @param owner_id integer 创建者ID
     * @return group_id integer 群组ID
     */
    public function add() {

        $name = $this->request->post('name');
        $owner_id = $this->auth->id;
        $data = ['name'=>$name, 'owner_id'=>$owner_id];
        //检验数据
        $validate = Loader::validate('Group');
        if (!$validate->scene('add')->check($data)) $this->error($validate->getError());
        //保存
        $group = new Group();
        if ($group->check_duplicate($name, $owner_id)) $this->error(__('Duplicate data'),[], 2401);
        $group_id = $group->add($name,$owner_id,$this->auth->getUserinfo());

        if (!$group_id )
            $this->error(__('Post error'),[],2402);
        else
            $this->success('', array_merge(Group::$fieldInfo, ['group_id'=> $group_id]));

    }

    /**
     * 关闭群组
     * @param $group_id Integer 群组ID
     * @param $owner_id Integer 创建者ID
     * @return Array [code=1:成功；code=0:失败] 是否关闭成功
     */
    public function close() {

        if (!$this->request->post('id')) $this->error(__('Group_id is missing'),[],2405);
        $id = (int)$this->request->post('id');
        $owner_id = $this->auth->id;

        $group = new Group();
        //检验数据
        $data = ['id'=> $id, 'owner_id'=> $owner_id];
        if (!$group->where($data)->find()) $this->error(__('Only owner can delete'),[],2403);
        //更新状态
        $data['status'] = 2; // (1:正常；2:关闭)
        $result = $group->save($data, ['id'=> $id]);

        if ($result !== 1 )
            $this->error(__('Update error'),[],2404);
        else
            $this->success('', array_merge(Group::$fieldInfo));

    }

    /**
     * 编辑群组信息
     * @param $name varchar 群组名称
     * @param $avatar varchar 群组封面图URL地址
     * @param $group_id Integer 群组ID
     * @param $owner_id Integer 创建者ID
     * @return Array [code=1:成功；code=0:失败] 是否编辑成功
     */
    public function edit() {

        $id = (int)$this->request->post('id');
        $name = $this->request->post('name');
        $avatar = $this->request->post('avatar');
        $owner_id = $this->auth->id;

        $group = new Group();

        //检测数据
        $data = [
            'id'=>$id,
            'owner_id'=>$owner_id
        ];
        if (!$group->where($data)->find()) $this->error(__('Only owner can edit'),[],2406);

        //校验数据
        $data = array_merge($data, ['name'=>$name, 'avatar'=>$avatar]);
        $validate = Loader::validate('Group');
        if (!$validate->scene('edit')->check($data)) $this->error($validate->getError());

        //更新状态
        $result = $group->save($data, ['id'=> $id]);

        if ($result !== 1 )
            $this->error(__('Update error'),[],2404);
        else
            $this->success('', array_merge(Group::$fieldInfo));

    }

    /**
     * 获取指定群组信息
     * @param $id Integer 群组ID
     * @param $user_id 申请查看人的user_id
     * @return $info Array 指定群组信息
     */
    public function info() {

        $group_id = (int)$this->request->post('id');
        $user_id = $this->auth->id;

        $group = new Group();
        $groupMember = new GroupMember();

        //查看权限的检测
        if (!$group->check_auth($group_id, $user_id)) $this->error(__('Only manager or member can read'),[], 2407);

        //查看群组信息
        $data = $group->info($group_id);

        if (!$data)
            $this->error(__('Update error'),[],2404);
        else
            $result = [
                'group_fieldInfo' => Group::$fieldInfo,
                'groupMember_fieldInfo' => GroupMember::$fieldInfo,
                'info' => $data
            ];

            $this->success('', $result);
    }

    /*
     * 获取群组列表
     * @method post
     * @param user_id 用户ID
     * @return Array group_fieldInfo:群组字段说明；groupMember_fieldInfo:群组成员字段说明；info[group_base]:用户在群组的角色；info[group_list]:群组列表
     */
    public function index(){

        $user_id = $this->auth->id;

        $group = new Group();
        $group_member = new GroupMember();

        //查看群组信息
        $data = $group->getList($user_id);

        if (!$data)
            $this->error(__('Update error'),[],2404);
        else
            $result = [
                'group_fieldInfo' => Group::$fieldInfo,
                'groupMember_fieldInfo' => GroupMember::$fieldInfo,
                'info' => $data
            ];

        $this->success('', $result);

    }

    public function transfer_ownership(){

        //TODO::群组转让
    }

    public function config(){

        $id = (int)$this->request->post('id');
        $config = $this->request->post('config');
        $user_id = $this->auth->id;

        $group = new Group();

        //检测数据
        $data = [
            'id'=>$id,
            'owner_id'=>$user_id
        ];
        if (!$group->where($data)->find()) $this->error(__('Only owner can edit'),[],2406);

        //校验数据
        $data = array_merge($data, ['config'=>$config]);
        $validate = Loader::validate('Group');
        if (!$validate->scene('config')->check($data)) $this->error($validate->getError());

        //更新状态
        $result = $group->save($data, ['id'=> $id]);

        if ($result !== 1 )
            $this->error(__('Update error'),[],2404);
        else
            $this->success('', array_merge(Group::$fieldInfo));

    }

}