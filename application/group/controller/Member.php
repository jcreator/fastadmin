<?php
namespace app\group\controller;

/**
 * Class Member
 * @author Rock
 * @package app\group\controller
 * 群组成员操作
 */


use think\Loader;
use think\Db;
use app\group\model\Group;
use app\group\model\GroupMember;
use app\group\model\GroupMemberImport;
use app\group\model\GroupAdmin;


class Member extends Base {

    protected $noNeedLogin = [];
    protected $token = '';

    public function _initialize()
    {
        parent::_initialize();

    }

    /*
     * 向指定群组导入数据
     * @param [$data：导入的成员多维数组，包含group_id, username,realname,email,mobile]
     * $return [$pici, $info, $false_info] 导入的批次号，成功的记录数组、失败的记录数组
     */
    public function import() {

        // 接收数据
        if (!$this->request->post('data')) $this->error(__('Post data is empty'),[],2408);
        $import_data = json_decode($this->request->post('data'), true);

        /*
        $test = [
                0 => ['group_id'=>13, 'username'=>'1905030201', 'realname'=>'张三', 'email'=>'liu@qq.com', 'mobile'=>'13357325199'],
                1 => ['group_id'=>13, 'username'=>'1905030202', 'realname'=>'李四', 'email'=>'liu2@qq.com', 'mobile'=>'13574092336'],
        ];
        $test2 = json_encode($test, JSON_UNESCAPED_UNICODE);
        */

        $count = 0;
        $info = [];
        $false_info = [];
        $data = [];
        $groupMemberModel = new GroupMember();

        // 验证数据
        foreach ( $import_data as $item ) {
            $validate = Loader::validate('GroupMember');
            if (!$validate->scene('group_member_import')->check($item)) {
                array_push($false_info, ['errMsg' => $validate->getError, 'info' => $item]);
            } else {
                $id = $groupMemberModel->check_duplicate($item['group_id'], $item['username']);
                if ($id) {
                    // 数据已存在
                    array_push($false_info, ['errMsg' => '数据已存在', 'info' => $item]);
                } else {
                    // 新增
                    array_push($info, $item);
                }
            }
        }

        // 批量导入数据
        if (count($info)>0){
            Db::startTrans();
            try{
                $data = $groupMemberModel->saveAll($info, false);
                Db::commit();
            } catch (\Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            $data = collection($data)->toArray();
        }

        // 生成批次
        $groupMemberImportModel = new GroupMemberImport();
        $res = $groupMemberImportModel->add($data);
        if ($res) {
            $pici = $res;
        } else {
            $pici = 0;
        }
        $this->success('', ['pici'=>$pici, 'info'=>$data, 'false_info'=>$false_info]);

    }

    /*
     * 获取成员列表
     * @method post
     * @param group_id 群组ID
     * @return Array groupMember_fieldInfo:群组成员字段说明；info:群组列表
     */
    public function index()
    {

        $group_id = $this->request->post('group_id');
        if (!$group_id) $this->error(__('Group_id is missing'),[],2405);
        $user_id = $this->auth->id;

        // index权限检查
        $group_member_model = new GroupMember();
        if (!$group_member_model->check_index_auth($group_id, $user_id)) $this->error(__('Only manager or member can read'),[],2407);

        $data = Db::name('group_member')->field($group_member_model->indexField)->where(['group_id'=>$group_id])->select();

        $this->success('', ['info'=> $data, 'groupMember_fieldInfo'=>GroupMember::$fieldInfo]);

    }

    /*
     * 获取指定成员信息
     * @param $member_id integer 指定成员ID
     * @param $user_id integer 申请查看人的user_id
     * @return $info Array 指定成员信息
     */
    public function info() {

        $group_id = (int)$this->request->post('group_id');
        $member_id = (int)$this->request->post('member_id');
        if (!$group_id or !$member_id) $this->error(__('param is empty'),[],2400);
        $user_id = $this->auth->id;

        $group_member_model = new GroupMember();

        //查看权限的检测
        if (!$group_member_model->check_index_auth($group_id, $user_id)) $this->error(__('Only manager or member can read'),[],2407);

        //查看群组信息
        $data = Db::name('group_member')->field($group_member_model->indexField)->where(['id'=>$member_id, 'status'=>1])->find();

        $this->success('', ['info'=> $data, 'groupMember_fieldInfo'=>GroupMember::$fieldInfo]);

    }

    /*
     * 删除指定成员
     * @param $group_id integer 群组ID
     * @param $member_id integer 指定成员ID
     * @param $user_id integer 申请人的user_id
     * @return bool 删除是否成功
     */
    public function del() {

        $group_id = (int)$this->request->post('group_id');
        $member_id = (int)$this->request->post('member_id');
        if (!$group_id or !$member_id) $this->error(__('param is empty'),[],2400);
        $user_id = $this->auth->id;

        $group_member_model = new GroupMember();

        //查看权限的检测
        if (!$group_member_model->check_del_auth($group_id, $user_id)) $this->error(__('Only manager can do'),[],2408);

        //查看群组信息
        $result = $group_member_model->allowField(['status'])->save(['status'=>3],['id'=>$member_id]);

        if ($result) {
            $this->success('', []);
        }else{
            $this->error(__('Post error'),[],2402);
        }

    }

    /*
     * 自行退群
     * @param $group_id integer 群组ID
     * @param $user_id integer 申请人的user_id
     * @return bool 删除是否成功
     */
    public function self_exit() {

        $group_id = (int)$this->request->post('group_id');
        if (!$group_id) $this->error(__('param is empty'),[],2400);
        $user_id = $this->auth->id;

        $group_member_model = new GroupMember();

        //查看群组信息
        $result = $group_member_model->allowField(['status'])->save(['status'=>2],['user_id'=>$user_id, 'group_id'=>$group_id]);

        if ($result) {
            $this->success('', []);
        }else{
            $this->error(__('Post error'),[],2402);
        }

    }

    /*
     * 编辑指定成员信息
     * @param Array 需修改的信息
     * @return Array [code=1:成功；code=0:失败] 是否编辑成功
     */
    public function edit() {

        $group_id = $this->request->post('group_id');
        $member_id = $this->request->post('member_id');

        /*
        $test = ['group_id'=>13, 'username'=>'1905030202', 'realname'=>'李四2', 'email'=>'liu22@qq.com', 'mobile'=>'13574092337'];
        $test = json_encode($test, JSON_UNESCAPED_UNICODE);
        */

        $data = $this->request->post('data');
        if ($data) $data = json_decode($data, true);
        if (!is_array($data) or empty($member_id) or empty($group_id)) $this->error(__('Post error'),[],2402);

        $user_id = $this->auth->id;

        $group_member_model = new GroupMember();

        //编辑权限的检测
        $res1 = $group_member_model->check_edit_auth($group_id, $user_id);
        $res2 = $group_member_model->is_self($member_id, $user_id);

        if (!$res1 or !$res2) $this->error(__('Visit limit'),[],2409);

        $validate = Loader::validate('GroupMember');
        if (!$validate->scene('group_member_edit')->check($data))  $this->error($validate->getError);

        $res = $group_member_model->allowField($group_member_model->editField)->save($data, ['id'=>$member_id]);
        if ( $res !== 1 )
            $this->error(__('Update error'),[],2404);
        else
            $this->success('', []);

    }

    /*
     * 群组成员发起，申请群组管理员
     */
    public function apply_manager()
    {

        $group_id = (int)$this->request->post('group_id');
        if (!$group_id) $this->error(__('param is empty'),[],2400);
        $user_id = $this->auth->id;

        // 权限检测
        if ( !Db::name('group_member')->where(['user_id'=>$user_id, 'group_id'=>$group_id, 'status'=>2])->find()) $this->error(__('Only member can do'),[],2410);
        if ( Db::name('group_admin')->where(['user_id'=>$user_id, 'group_id'=>$group_id, 'status'=>2])->find()) $this->error(__('Already an admin'),[],2411);

        // 新增申请
        $group_admin_apply_model = new GroupAdminApply();
        $data = $group_admin_apply_model->save(['user_id'=>$user_id, 'group_id'=>$group_id]);

        if ($data){
            $this->success('', []);
        } else {
            $this->error(__('Post error'), [], 2402);
        }

    }

    /*
     * 尘缘发起，获取自己提交审核的清单
     * @return array 自己提交审批的申请人及群组信息列表
     */
    public function apply_list_self()
    {

        $user_id = $this->auth->id;

        // 获取指定群组的申请人信息
        $group_admin = new GroupAdmin();
        $res = $group_admin->with('admin_info')->where('user_id',$user_id)->select();
        $res = collection($res)->toArray();

        $this->success('', ['info'=>$res, 'fieldInfo'=> $group_admin->fieldInfo]);

    }

    /*
     * 管理员发起，获取待审核的清单
     * @return array 待审批的申请人及群组信息列表
     */
    public function apply_list()
    {

        $user_id = $this->auth->id;

        // 获取请求人管理的群组id数组
        $group_ids = Db::name('group_admin')->where(['user_id'=>$user_id, 'status'=>2])->column('group_id');

        // 获取指定群组的申请人信息
        $where['group_id'] = ['in', $group_ids];
        $where['status'] = ['eq', 1];
        $res = GroupAdmin::with('user_info')->where($where)->select();
        $res = collection($res)->toArray();

        $this->success('', $res);

    }

    /*
     * 管理员发起，审批申请信息
     * @param $apply_id integer 申请单id
     * @param $info array 审批信息[msg, status] =2 表示审核通过，=3 表示审核不通过
     * @return 标准返回值（[code, msg, data]）
     */
    public function approval_apply()
    {

        $apply_id = (int)$this->request->post('apply_id');
        $info = $this->request->post('info');
        if ($info) $info = json_decode($info, true);
        if (!$apply_id or !is_array($info)) $this->error(__('param is empty'),[],2400);

        $user_id = $this->auth->id;
        $apply = GroupAdmin::get($apply_id);
        if (!$apply) $this->error(__('Record is missing'),[],2414);

        // 权限检测
        $res = GroupAdmin::where(['user_id'=>$user_id, 'group_id'=>$apply->group_id, 'status'=>2])->find();
        if ($res === null) $this->error(__('Only manager can do'),[],2408);

        // 数据检测
        if ($apply->status === 2) $this->error(__('Others have already dealt with it'),'',2413);

        // 审核数据->group_admin
        $data['auditor_id'] = $user_id;
        $data['auditor_msg'] = $info['msg'];
        $data['status'] = $info['status'];
        $result = GroupAdmin::where('id', $apply->id)->update($data);

        if ($result) $this->success('', $data);
        else $this->error();

    }

    public function transfer_ownership(){

        //TODO::群组转让
    }

    public function config(){

        $id = (int)$this->request->post('id');
        $config = $this->request->post('config');
        $user_id = $this->auth->id;

        $group = new Group();

        //检测数据
        $data = [
            'id'=>$id,
            'owner_id'=>$user_id
        ];
        if (!$group->where($data)->find()) $this->error(__('Only owner can edit'),[],2406);

        //校验数据
        $data = array_merge($data, ['config'=>$config]);
        $validate = Loader::validate('Group');
        if (!$validate->scene('config')->check($data)) $this->error($validate->getError());

        //更新状态
        $result = $group->save($data, ['id'=> $id]);

        if ($result !== 1 )
            $this->error(__('Update error'),[],2404);
        else
            $this->success('', array_merge($group->getFieldInfo()));

    }

    /*
     * 入群申请
     */
    public function apply_member()
    {

        $group_id = (int)$this->request->post('group_id');
        $info = $this->request->post('info');
        if ($info) $info = json_decode($info, true);
        if (!$group_id or !is_array($info)) $this->error(__('param is empty'),[],2400);

        $user_id = $data['user_id'] = $this->auth->id;
        if ($this->auth->username) $data['username'] = $this->auth->username;
        if ($this->auth->nickname) $data['nickname'] = $this->auth->nickname;
        if ($this->auth->realname) $data['realname'] = $this->auth->realname;
        if ($this->auth->avatar) $data['avatar'] = $this->auth->avatar;
        if ($this->auth->email) $data['email'] = $this->auth->email;
        if ($this->auth->mobile) $data['mobile'] = $this->auth->mobile;
        $data['group_id'] = $group_id;
        $data['apply_msg'] = $info['msg'];

        // 查看是否在黑名单
        $group = Group::get($group_id);
        if (!$group) $this->error(__('Record is missing'),[],2414);
        $black_list = explode(',',$group->blacklist);
        if (is_array($black_list) and in_array($user_id, $black_list)) $this->error(__('In the blacklist'),[], 2415);

        // 查看是否在member中
        $group_member_model = new GroupMember();
        $member = $group_member_model->where(['user_id'=>$user_id, 'group_id'=>$group_id])->find();
        if ($member and $member->status === 2) $this->error(__('Already a member'),[],2416);
        if ($member and $member->status === 1) $this->error(__('Is applying'),[],2412);

        $result = $group_member_model->allowField(true)->save($data);

        $result ? $this->success() : $this->error();

    }

    /*
     * 申请人调用：查看自己入群申请列表
     */
    public function apply_member_list_self()
    {

        $user_id = $data['user_id'] = $this->auth->id;

        $group_member_model = new GroupMember();
        $res = $group_member_model->where(['user_id'=>$user_id, 'status'=>1])->select();
        if ($res) $res = collection($res)->toArray();
        else $res = [];

        $this->success('',['info'=>$res, 'fieldInfo'=>$group_member_model->getFieldInfo()]);

    }

    /*
     * 管理员发起，获取待审核的成员清单
     * @return array 待审批的申请人及群组信息列表
     */
    public function apply_member_list()
    {

        $user_id = $this->auth->id;

        // 获取请求人管理的群组id数组
        $group_ids = Db::name('group_admin')->where(['user_id'=>$user_id, 'status'=>2])->column('group_id');

        // 获取指定群组的申请人信息
        $where['group_id'] = ['in', $group_ids];
        $where['status'] = ['eq', 1];
        $res = GroupMember::with('user_info')->with('group_info')->where($where)->select();
        if ($res) $res = collection($res)->toArray();
        else $res = [];

        $this->success('', ['info'=>$res, 'member_field_info'=>GroupMember::$fieldInfo, 'group_field_info'=>Group::$fieldInfo]);

    }

    /*
     * 管理员发起，审批申请入群信息
     * @param $apply_id integer 申请单id
     * @param $info array 审批信息[msg, status] =2 表示审核通过，=3 表示审核不通过
     * @return 标准返回值（[code, msg, data]）
     */
    public function approval_apply_member()
    {

        $apply_id = (int)$this->request->post('apply_id');
        $info = $this->request->post('info');
        if ($info) $info = json_decode($info, true);
        if (!$apply_id or !is_array($info)) $this->error(__('param is empty'),[],2400);

        $user_id = $this->auth->id;
        $apply = GroupMember::get($apply_id);
        if (!$apply) $this->error(__('Record is missing'),[],2414);

        // 权限检测
        $res = GroupAdmin::where(['user_id'=>$user_id, 'group_id'=>$apply->group_id, 'status'=>2])->find();
        if ($res === null) $this->error(__('Only manager can do'),[],2408);

        // 数据检测
        if ($apply->status === 2) $this->error(__('Others have already dealt with it'),'',2413);

        // 审核数据->group_admin
        $data['auditor_id'] = $user_id;
        $data['approval_msg'] = $info['msg'];
        $data['status'] = $info['status'];
        $group_member_model = new GroupMember();
        $result = $group_member_model->save($data, ['id'=>$apply->id]);

        if ($result) $this->success('', $data);
        else $this->error();

    }

}