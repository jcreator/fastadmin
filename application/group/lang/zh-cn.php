<?php

return [
    'param is empty'                => '2400:请求参数不能为空',
    'Duplicate data'                => '2401:重复数据，提交失败',
    'Post error'                    => '2402:提交数据失败',
    'Only owner can delete'         => '2403:只有创建者才能删除',
    'Update error'                  => '2404:更新数据失败',
    'Group_id is missing'           => '2405:群组ID未能传递',
    'Only owner can edit'           => '2406:只有创建者才能编辑',
    'Only manager or member can read' => '2407:只有管理员或成员才能查看',
    'Only manager can do'           => '2408:只有管理员可以操作',
    'Visit limit'                   => '2409:访问受限',
    'Only member can do'            => '2410:只有群组成员可以操作',
    'Already an admin'              => '2411:已经是管理员，无需重新申请',
    'Is applying'                   => '2412:正在申请，无需重复提交',
    'Others have already dealt with it'=> '2413:其他管理员已经处理此申请',
    'Record is missing'             => '2414:记录不存在',
    'In the blacklist'              => '2415:该用户已被列入黑名单',
    'Already a member'              => '2416:该用户已经是正式成员'
];
