<?php

namespace app\group\model;

use think\Cache;
use think\Db;
use think\model;

class GroupMemberImport extends model
{
    protected $name = 'group_member_import';

    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    public function add($info)
    {
        if (count($info) == 0) return (['code'=>0, 'msg'=>'生成批次的数据为空']);
        $data = [];
        foreach ($info as $item){
            array_push($data, $item['id']);
        }
        $result = $this->save(['member_ids'=>implode(',',$data)]);
        if ($result){
            return $this->getLastInsID();
        } else {
            return false;
        }

    }


}