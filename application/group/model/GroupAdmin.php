<?php

namespace app\group\model;

use think\Cache;
use think\Db;
use think\model;

class GroupAdmin extends model
{
    protected $name = 'group_admin';

    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    public $fieldInfo = [
        'status'    => ['1'=>'申请中', '2'=>'审核通过', '3'=>'审核未通过', '4'=>'已退出', '5'=>'已删除']
    ];

    public function userInfo()
    {
        return $this->hasOne('app\admin\model\User','id','user_id')->field('id, username, nickname, realname, avatar, prevtime, logintime, jointime');
    }

    public function adminInfo()
    {
        return $this->hasOne('app\admin\model\User','id','auditor_id')->field('id, username, nickname, realname, avatar, prevtime, logintime, jointime');
    }

    public function check_duplicate($user_id, $group_id){
        $where['user_id'] = ['eq', $user_id];
        $where['group_id'] = ['eq', $group_id];
        $result = $this->where($where)->find();
        if ($result) return true;
        else return false;
    }

    public function add( $user_id, $group_id ) {

        if (!$this->check_duplicate($user_id, $group_id)){
            $data['user_id'] = $user_id;
            $data['group_id'] = $group_id;
            $this->save($data);
        }

    }

}