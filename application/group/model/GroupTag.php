<?php

namespace app\group\model;

use think\Cache;
use think\Db;
use think\model;

class GroupTag extends model
{
    protected $name = 'group_tag';

    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    //protected $field = ['id, name, owner_id, parent_id, num_member, num_message, avatar, config, create_time, update_time, type, status'];

    static public $fieldInfo = [
        'type'      => ['1'=>'官方','2'=>'自定义'],
        'status'    => ['1'=>'正常', '2'=>'关闭']
    ];

    protected $defaultConfig = [
        'create_must_realname'      => ['title'=>'实名认证用户才能建群', 'value'=> 0],
        'count_create'              => ['title'=>'缺省创建群组数量', 'value'=> 3],
        'mustSetNickname_invite'    => ['title'=>'邀请入群强制设置别名', 'value'=> 0],
        'enterGroup_must_realname'  => ['title'=>'用户实名认证后方能进入该群组', 'value'=> 0]
    ];

    public function getList($user_id) {

        $group_base = Db::name('group_member')
            ->where('user_id','eq',$user_id)
            ->field('id, group_id, role')
            ->order('role, update_time desc')
            ->select();
        if (!$group_base) return false;
        $group_ids = '';
        foreach ($group_base as $item){
            $group_ids .= $item['group_id'] . ',';
        }

        $group_list = Db::name('group')
            ->where('id','in',$group_ids)
            ->field($this->field)
            ->order('type, update_time desc')
            ->select();
        if (!$group_list) return false;

        return ['group_base'=>$group_base, 'group_list'=>$group_list];

    }


}