<?php

namespace app\group\model;

use think\Cache;
use think\Db;
use think\model;

class GroupMember extends model
{
    protected $name = 'group_member';

    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    public $indexField = 'id, group_id, user_id, username, nickname, realname, avatar,email, mobile, config, create_time, update_time,role, status';
    public $editField = ['username', 'nickname', 'realname', 'avatar', 'email', 'mobile'];

    static public $fieldInfo = [
        'role'      => ['1'=>'创建者','2'=>'管理员','3'=>'成员'],
        'status'    => ['1'=>'正在审核', '2'=>'已审核通过', '3'=>'审核不通过','4'=>'已退出', '5'=>'已删除']
    ];

    public function userInfo()
    {
        return $this->hasOne('app\admin\model\User','id','user_id')->field('id, username, nickname, realname, avatar, prevtime, logintime, jointime');
    }

    public function groupInfo()
    {
        return $this->hasOne('Group','id','group_id');
    }

    public function check_duplicate($group_id, $username){
        $where['group_id'] = ['eq', $group_id];
        $where['username'] = ['eq', $username];
        $result = $this->where($where)->find();
        if ($result) {
            $result = $result->toArray();
            return $result['id'];
        }
        else return false;
    }

    public function check_index_auth($group_id, $user_id)
    {
        $where['group_id'] = ['eq', $group_id];
        $where['user_id'] = ['eq', $user_id];

        $res1 = $this->where($where)->find();
        $res2 = Db::name('group_admin')->where($where)->find();

        if ($res1 or $res2) return true;
        else return false;
    }

    /*
     * 检测群组内删除成员的权限
     */
    public function check_del_auth($group_id, $user_id)
    {
        $where['group_id'] = ['eq', $group_id];
        $where['user_id'] = ['eq', $user_id];

        $res = Db::name('group_admin')->where($where)->find();

        if ($res) return true;
        else return false;
    }

    /*
     * 检测群组内编辑成员的权限
     */
    public function check_edit_auth($group_id, $user_id)
    {
        $where['group_id'] = ['eq', $group_id];
        $where['user_id'] = ['eq', $user_id];

        $res = Db::name('group_admin')->where($where)->find();

        if ($res) return true;
        else return false;
    }

    /*
     * 检测群组内编辑成员的权限
     */
    public function is_self($member_id, $user_id)
    {
        $where['id'] = ['eq', $member_id];
        $where['user_id'] = ['eq', $user_id];

        $res = $this->where($where)->find();

        if ($res) return true;
        else return false;
    }

    public function add($group_id, $userInfo, $role=3 ) {

        $data['group_id'] = $group_id;
        $data['user_id'] = $userInfo['id'];
        if (isset($userInfo['username'])) $data['username'] = $userInfo['username'];
        if (isset($userInfo['nickname'])) $data['nickname'] = $userInfo['nickname'];
        if (isset($userInfo['realname'])) $data['realname'] = $userInfo['realname'];
        if (isset($userInfo['avatar'])) $data['avatar'] = $userInfo['avatar'];
        $data['role'] = $role;

        // 检测数据是否已存在
        $id = $this->check_duplicate($group_id, $userInfo['id']);
        $validate = Loader::validate('GroupMember');
        if (!$id) {
            // 检测数据合法性
            if (!$validate->scene('add_admin')->check($data)) $this->error($validate->getError());
            $this->save($data);
        } else {
            if (!$validate->scene('edit')->check($data)) $this->error($validate->getError());
            $data['id'] = $id;
            $this->save($data, ['id'=>$id]);
        }

    }


}