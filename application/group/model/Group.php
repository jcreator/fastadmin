<?php

namespace app\group\model;

use think\Cache;
use think\Db;
use think\model;

class Group extends model
{
    protected $name = 'group';

    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    //protected $field = ['id, name, owner_id, parent_id, num_member, num_message, avatar, config, create_time, update_time, type, status'];

    static public $fieldInfo = [
        'type'      => ['1'=>'自定义','2'=>'官方认证'],
        'status'    => ['1'=>'正常', '2'=>'关闭']
    ];

    protected $defaultConfig = [
        'create_must_realname'      => ['title'=>'实名认证用户才能建群', 'value'=> 0],
        'count_create'              => ['title'=>'缺省创建群组数量', 'value'=> 3],
        'mustSetNickname_invite'    => ['title'=>'邀请入群强制设置别名', 'value'=> 0],
        'enterGroup_must_realname'  => ['title'=>'用户实名认证后方能进入该群组', 'value'=> 0]
    ];

    /**
     * 检查重复存在重复记录
     * @param $name 群组名称
     * @param $owner_id 创建者ID
     * @return bool (true:存在；false:不存在)
     */
    public function check_duplicate($name, $owner_id){
        $where['name'] = ['eq', $name];
        $where['owner_id'] = ['eq', $owner_id];
        $result = $this->where($where)->find();
        if ($result) return true;
        else return false;
    }

    /**
     * 检查用户ID在指定群组ID是否有查看权限
     * @param $group_id
     * @param $user_id
     * @return bool(true:有; false:没有)
     */
    public function check_auth($group_id, $user_id){

        $where['group_id'] = ['eq', $group_id];
        $where['user_id'] = ['eq', $user_id];
        if (!Db::name('group_member')->where($where)->find()) return false;
        if (!Db::name('group_admin')->where($where)->find()) return false;
        return true;

    }

    /**
     * 创建群组
     * @param $name 群组名称
     * @param $owner_id 创建者ID
     * @param array $userInfo 创建者用户信息
     * @return group_id|false integer|boolean 创建的群组ID
     */
    public function add($name, $owner_id, $userInfo = []) {

        Db::startTrans();
        try {

            $data = [
                'name'          => $name,
                'owner_id'      => $owner_id,
                'config'        => json_encode($this->defaultConfig, JSON_UNESCAPED_UNICODE)
            ];
            $this->save($data);
            $group_id = $this->getLastInsID();
            $group_member = new GroupMember();
            $group_member->add($group_id, $userInfo, 1);
            $group_admin = new GroupAdmin();
            $group_admin->add($userInfo['id'], $group_id);

            Db::commit();

        } catch (\Exception $e){
            Db::rollback();
            $this->error($e->getMessage(),[], 2402);
        }

        return $group_id;

    }

    /*
     * 查看指定群组及成员信息
     * @param $group_id integer 群组ID
     * @return Array groupInfo:群组信息，groupMemberInfo:群组成员信息
     */
    public function info($group_id){

        if ($this->where('id','eq',$group_id)->find()){

            $groupMemberInfo = [];

            $groupInfo = $this->where('id','eq',$group_id)->find()->toArray();
            $group_member = new GroupMember();
            if ($group_member->where('group_id','eq',$group_id)->select()){
                $groupMemberInfo = collection($group_member->where('group_id','eq',$group_id)->select())->toArray();
            }
            return ['groupInfo'=>$groupInfo, 'groupMemberInfo'=>$groupMemberInfo];
        }

        return false;

    }

    public function getList($user_id) {

        $group_base = Db::name('group_member')
            ->where('user_id','eq',$user_id)
            ->field('id, group_id, role')
            ->order('role, update_time desc')
            ->select();
        if (!$group_base) return false;
        $group_ids = '';
        foreach ($group_base as $item){
            $group_ids .= $item['group_id'] . ',';
        }

        $group_list = Db::name('group')
            ->where('id','in',$group_ids)
            ->field($this->field)
            ->order('type, update_time desc')
            ->select();
        if (!$group_list) return false;

        return ['group_base'=>$group_base, 'group_list'=>$group_list];

    }


}