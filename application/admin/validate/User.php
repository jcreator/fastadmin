<?php

namespace app\admin\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'group_id|群组ID'      => 'require|integer',
        'username|用户名'      => 'length:4,50|alphaDash',
        'nickname|用户别名'     => 'length:4,50|chsDash',
        'realname|真实姓名'     => 'length:2,50|chsDash',
        'password|密码'        => 'length:4,32|alphaDash',
        'email|邮箱'           => 'email',
        'mobile|手机'          => 'length:11|number',
        'avatar|用户头像'       => 'url',
        'joinip|IP'           => 'ip'

    ];

    protected $message = [
        'group_id.require'  =>  '群组ID不能为空',
        'group_id.integer'  =>  '群组ID必须为整数',
        'username.length'   =>  '用户名长度必须在 4至50个字符之间',
        'username.alphaDash'  =>  '用户名只能为汉字,字母,数字,下划线,破折号',
        'nickname.length'   =>  '用户别名长度必须在 4至50个字符之间',
        'nickname.chsDash'  =>  '用户别名只能为汉字,字母,数字,下划线,破折号',
        'realname.length'   =>  '真实姓名长度必须在2至50个字符之间',
        'realname.chsDash'  =>  '真实姓名只能为汉字,字母,数字,下划线,破折号',
        'password.length'   =>  '密码长度必须在4至32位字符',
        'password.alphaDash'=>  '用户别名只能为字母,数字,下划线,破折号',
        'email.email'       =>  '必须输入有效的邮箱地址',
        'mobile.length'     =>  '输入的手机位数必须为11位',
        'mobile.number'     =>  '输入的手机号必须为数字',
        'avatar.url'        =>  '用户头像地址必须为合法的URL地址',
        'joinip.ip'         =>  '必须输入合法的ip地址'
    ];

    protected $scene = [
        'group_member_add'       =>  ['username', 'realname', 'email', 'mobile'],
    ];
    
}
