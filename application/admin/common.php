<?php

use app\common\model\Category;
use fast\Form;
use fast\Tree;
use think\Db;

if (!function_exists('build_select')) {

    /**
     * 生成下拉列表
     * @param string $name
     * @param mixed $options
     * @param mixed $selected
     * @param mixed $attr
     * @return string
     */
    function build_select($name, $options, $selected = [], $attr = [])
    {
        $options = is_array($options) ? $options : explode(',', $options);
        $selected = is_array($selected) ? $selected : explode(',', $selected);
        return Form::select($name, $options, $selected, $attr);
    }
}

if (!function_exists('build_radios')) {

    /**
     * 生成单选按钮组
     * @param string $name
     * @param array $list
     * @param mixed $selected
     * @return string
     */
    function build_radios($name, $list = [], $selected = null)
    {
        $html = [];
        $selected = is_null($selected) ? key($list) : $selected;
        $selected = is_array($selected) ? $selected : explode(',', $selected);
        foreach ($list as $k => $v) {
            $html[] = sprintf(Form::label("{$name}-{$k}", "%s {$v}"), Form::radio($name, $k, in_array($k, $selected), ['id' => "{$name}-{$k}"]));
        }
        return '<div class="radio">' . implode(' ', $html) . '</div>';
    }
}

if (!function_exists('build_checkboxs')) {

    /**
     * 生成复选按钮组
     * @param string $name
     * @param array $list
     * @param mixed $selected
     * @return string
     */
    function build_checkboxs($name, $list = [], $selected = null)
    {
        $html = [];
        $selected = is_null($selected) ? [] : $selected;
        $selected = is_array($selected) ? $selected : explode(',', $selected);
        foreach ($list as $k => $v) {
            $html[] = sprintf(Form::label("{$name}-{$k}", "%s {$v}"), Form::checkbox($name, $k, in_array($k, $selected), ['id' => "{$name}-{$k}"]));
        }
        return '<div class="checkbox">' . implode(' ', $html) . '</div>';
    }
}


if (!function_exists('build_category_select')) {

    /**
     * 生成分类下拉列表框
     * @param string $name
     * @param string $type
     * @param mixed $selected
     * @param array $attr
     * @return string
     */
    function build_category_select($name, $type, $selected = null, $attr = [], $header = [])
    {
        $tree = Tree::instance();
        $tree->init(Category::getCategoryArray($type), 'pid');
        $categorylist = $tree->getTreeList($tree->getTreeArray(0), 'name');
        $categorydata = $header ? $header : [];
        foreach ($categorylist as $k => $v) {
            $categorydata[$v['id']] = $v['name'];
        }
        $attr = array_merge(['id' => "c-{$name}", 'class' => 'form-control selectpicker'], $attr);
        return build_select($name, $categorydata, $selected, $attr);
    }
}

if (!function_exists('build_toolbar')) {

    /**
     * 生成表格操作按钮栏
     * @param array $btns 按钮组
     * @param array $attr 按钮属性值
     * @return string
     */
    function build_toolbar($btns = NULL, $attr = [])
    {
        $auth = \app\admin\library\Auth::instance();
        $controller = str_replace('.', '/', strtolower(think\Request::instance()->controller()));
        $btns = $btns ? $btns : ['refresh', 'detail', 'add', 'edit', 'del', 'import'];
        $btns = is_array($btns) ? $btns : explode(',', $btns);
        $index = array_search('delete', $btns);
        if ($index !== FALSE) {
            $btns[$index] = 'del';
        }
        $btnAttr = [
            'refresh' => ['javascript:;', 'btn btn-primary btn-refresh', 'fa fa-refresh', '', __('Refresh')],
            'detail'  => ['javascript:;', 'btn btn-primary btn-detail', 'fa fa-list-alt', __('Detail'), __('Detail')],
            'add'     => ['javascript:;', 'btn btn-success btn-add', 'fa fa-plus', __('Add'), __('Add')],
            'edit'    => ['javascript:;', 'btn btn-success btn-edit btn-disabled disabled', 'fa fa-pencil', __('Edit'), __('Edit')],
            'del'     => ['javascript:;', 'btn btn-danger btn-del btn-disabled disabled', 'fa fa-trash', __('Delete'), __('Delete')],
            'import'  => ['javascript:;', 'btn btn-danger btn-import', 'fa fa-upload', __('Import'), __('Import')],
        ];
        $btnAttr = array_merge($btnAttr, $attr);
        $html = [];
        foreach ($btns as $k => $v) {
            //如果未定义或没有权限
            if (!isset($btnAttr[$v]) || ($v !== 'refresh' && !$auth->check("{$controller}/{$v}"))) {
                continue;
            }
            list($href, $class, $icon, $text, $title) = $btnAttr[$v];
            $extend = $v == 'import' ? 'id="btn-import-file" data-url="ajax/upload" data-mimetype="csv,xls,xlsx" data-multiple="false"' : '';
            $html[] = '<a href="' . $href . '" class="' . $class . '" title="' . $title . '" ' . $extend . '><i class="' . $icon . '"></i> ' . $text . '</a>';
        }
        return implode(' ', $html);
    }
}
if (!function_exists('build_toolbar2')) {
    /**
     * 重新修订：生成表格操作按钮栏
     * 将Uid考虑进来
     * @reversion by Rock
     * @param array $toolbarParams 参数
     * @return string
     */
    function build_toolbar2($toolbarParams)
    {
        $btns = $toolbarParams['btns'];
        $uid = $toolbarParams['uid'];
        $attr = $toolbarParams['attr'];
        $ruleList = $toolbarParams['ruleList'];

        $auth = \app\admin\library\Auth::instance();
        $controller = str_replace('.', '/', strtolower(think\Request::instance()->controller()));
        $btns = $btns ? $btns : ['refresh', 'detail', 'add', 'edit', 'del', 'import'];
        $btns = is_array($btns) ? $btns : explode(',', $btns);
        $index = array_search('delete', $btns);
        if ($index !== FALSE) {
            $btns[$index] = 'del';
        }
        $btnAttr = [
            'refresh' => ['javascript:;', 'btn btn-primary btn-refresh', 'fa fa-refresh', '', __('Refresh')],
            'detail'  => ['javascript:;', 'btn btn-primary btn-detail', 'fa fa-list-alt', __('Detail'), __('Detail')],
            'add'     => ['javascript:;', 'btn btn-success btn-add', 'fa fa-plus', __('Add'), __('Add')],
            'edit'    => ['javascript:;', 'btn btn-success btn-edit btn-disabled disabled', 'fa fa-pencil', __('Edit'), __('Edit')],
            'del'     => ['javascript:;', 'btn btn-danger btn-del btn-disabled disabled', 'fa fa-trash', __('Delete'), __('Delete')],
            'import'  => ['javascript:;', 'btn btn-danger btn-import', 'fa fa-upload', __('Import'), __('Import')],
        ];
        $btnAttr = array_merge($btnAttr, $attr);
        $html = [];
        foreach ($btns as $k => $v) {
            //如果未定义或没有权限
            if (!isset($btnAttr[$v]) || ($v !== 'refresh' && !$auth->check2("{$controller}/{$v}", $ruleList))) {
                continue;
            }
            list($href, $class, $icon, $text, $title) = $btnAttr[$v];
            $extend = $v == 'import' ? 'id="btn-import-file" data-url="ajax/upload" data-mimetype="csv,xls,xlsx" data-multiple="false"' : '';
            $html[] = '<a href="' . $href . '" class="' . $class . '" title="' . $title . '" ' . $extend . '><i class="' . $icon . '"></i> ' . $text . '</a>';
        }
        return implode(' ', $html);
    }
}

if (!function_exists('build_heading')) {

    /**
     * 生成页面Heading
     *
     * @param string $path 指定的path
     * @return string
     */
    function build_heading($path = NULL, $container = TRUE)
    {
        $title = $content = '';
        if (is_null($path)) {
            $action = request()->action();
            $controller = str_replace('.', '/', request()->controller());
            $path = strtolower($controller . ($action && $action != 'index' ? '/' . $action : ''));
        }
        // 根据当前的URI自动匹配父节点的标题和备注
        $data = Db::name('auth_rule')->where('name', $path)->field('title,remark')->find();
        if ($data) {
            $title = __($data['title']);
            $content = __($data['remark']);
        }
        if (!$content)
            return '';
        $result = '<div class="panel-lead"><em>' . $title . '</em>' . $content . '</div>';
        if ($container) {
            $result = '<div class="panel-heading">' . $result . '</div>';
        }
        return $result;
    }
}

if (!function_exists('multi_to_array')) {

      /**
       * Textarea多行数据转数组
       * @param String $str  输入数据
       * @param String $separator 分隔符
       * @return Array
       * @author Rock
       */
      function multi_to_array1($str='', $separator='=>')
      {
          $arr = explode("\n" , trim($str));
          if (!is_array($arr)) return false;
          $result = [];
          foreach ($arr as $v){
              list($key, $value) = explode($separator, $v);
              $value = explode(',',$value);
              $result[$key] = $value;
          }
          return $result;
      }
}


/**
 * 将指定字符串补全到指定字符串中的指定位置或与子串合并
 * @param $l1 包含需转换子字符串的字符串
 * @param $l2 补全部分
 * @return mixed 转换后的字符串
 */
function formatUrl($l1, $l2){
    if (preg_match_all("/(<img[^>]+src=\"([^\"]+)\"[^>]*>)|(<a[^>]+href=\"([^\"]+)\"[^>]*>)|(<img[^>]+src='([^']+)'[^>]*>)|(<a[^>]+href='([^']+)'[^>]*>)/i",$l1,$regs)){
        foreach($regs[0] as $num => $url){
            $l1 = str_replace($url,str_replace_lsl($url, $l2),$l1);
        }
    }
    return $l1;
}

function str_replace_lsl($l1,$l2)
{
    if (preg_match("/(.*)(href|src)\=(.+?)( |\/\>|\>).*/i",$l1,$regs)){
        $I2 = $regs[3];
    }
    if (strlen($I2)>0){
        $I1 = str_replace(chr(34),"",$I2);
        $I1 = str_replace(chr(39),"",$I1);
    } else {
        return $l1;
    }

    $url_parsed = parse_url($l2);
    $scheme = $url_parsed["scheme"];
    if ($scheme!="") { $scheme = $scheme."://"; }
    $host = $url_parsed["host"];
    $l3 = $scheme.$host;
    if ( strlen($l3)==0 ) { return $l1; }
    $path = '';
    if ( isset($url_parsed['path']) ){
        $path = dirname($url_parsed["path"]);
        if ( $path[0]=="\\") { $path=""; }
    }

    $pos = strpos($I1,"#");
    if ( $pos>0 ) $I1 = substr($I1,0,$pos);

    //判断类型
    if ( preg_match("/^(http|https|ftp):(\/\/|\\\\)(([\w\/\\\+\-~`@:%])+\.)+([\w\/\\\.\=\?\+\-~`@\':!%#]|(&)|&)+/i", $I1)){
        return $l1;
    } elseif ( $I1[0]=="/" ) {
        $I1 = $l3.$I1;
    } elseif ( substr($I1,0,3)=="../" ){//相对路径
        while ( substr($I1,0,3)=="../" ){
            $I1 = substr($I1, strlen($I1)-(strlen($I1)-3), strlen($I1)-3);
            if ( strlen($path)>0 ){
                $path = dirname($path);
            }
        }
        $I1 = $l3 . $path . "/" . $I1;
    } elseif ( substr($I1,0,2)=="./" ){
        $I1 = $l3 . $path . substr($I1,strlen($I1)-(strlen($I1)-1),strlen($I1)-1);
    } elseif (strtolower(substr($I1,0,7))=="mailto:"||strtolower(substr($I1,0,11))=="javascript:" ){
        return $l1;
    } else {
        $I1 = $l3 . $path . "/" . $I1;
    }
    return str_replace($I2, "\"$I1\"", $l1);
}
