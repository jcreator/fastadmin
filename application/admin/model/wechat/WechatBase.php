<?php

namespace app\admin\model\wechat;

use think\Model;

class WechatBase extends Model
{
    // 表名
    protected $name = 'wechat_base';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    public function entity()
    {
        return $this->belongsTo('app\common\model\Entity');
    }
}
