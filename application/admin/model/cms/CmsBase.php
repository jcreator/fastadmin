<?php

namespace app\admin\model\cms;

use think\Model;

class CmsBase extends Model
{

    // 单独配置数据库连接
    protected $connection = 'db_config_extra';
}