<?php

namespace app\admin\controller\wechat;

use app\common\model\Entity;
use app\common\controller\Backend;
use app\common\model\WechatResponse;
use EasyWeChat\Foundation\Application;
use think\Exception;
use think\model\Collection;

/**
 * 公众号管理
 * @author Rock1978
 * @version 1.0.1
 * @icon fa fa-list-alt
 */
class Index extends Backend
{

    protected $wechatcfg = NULL;
    protected $model = NULL;
    protected $type = 'wechat';
    protected $pid = 0;
    protected $flag = '';

    public function _initialize()
    {
        parent::_initialize();

        //对应模型实例化
        $this->model = new Entity();
        // 设置过滤条件
        $this->dataLimit = true; // 先查创建者数据
        $filter['type'] = ['eq', $this->type];
        $this->model->setFilter($filter);

        /*
        $this->pid = 242;
        $this->flag = 'wechat_entity';
        $groupAccessList = $this->model->getGroupAccessList($this->groupAccessList, $this->pid, $this->flag);
        // 根据uid对应entityID，对数据进行初步过滤
        $filter = [];
        if (count($groupAccessList))
        {
            $ids = array_keys($groupAccessList);
            $filter['id'] = ['in', $ids];
        }
        $this->model->setFilter($filter);
        */

    }

    /**
     * 公众号列表
     */
    public function index()
    {
        $responselist = array();
        $all = WechatResponse::all();
        foreach ($all as $k => $v) {
            $responselist[$v['eventkey']] = $v['title'];
        }

        //初始化工具条
        $toolbarParams = [
            'btns' => ['refresh', 'detail', 'add', 'edit', 'del'],
            'uid' => '',
            'attr' => [],
            'ruleList' => $this->ruleList
        ];

        $this->view->assign('toolbarParams', $toolbarParams);
        $this->view->assign('responselist', $responselist);
        return $this->view->fetch();
    }

    public function data()
    {
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->type()
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->type()
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
    }

    /**
     * 新增
     */
    public function add()
    {
        if ($this->request->isPost()) {

            $data_entity = $this->request->post("row/a");
            $data_connect = $this->request->post("connect/a");
            if (!empty($data_entity) and !empty($data_connect)) {
                foreach ($data_entity as $k => &$v) {
                    $v = is_array($v) ? implode(',', $v) : $v;
                }
                $entity_extend = ['admin_id'=>$this->admin['id'], 'icon'=>'fa fa-wechat', 'type'=>'wechat', 'status'=>'unauthorized'];
                $data_entity = array_merge($data_entity, $entity_extend);

                foreach ($data_connect as $k => &$v) {
                    $v = is_array($v) ? implode(',', $v) : $v;
                }
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = basename(str_replace('\\', '/', get_class($this->model)));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : true) : $this->modelValidate;
                        $this->model->validate($validate);
                    }
                    $result1 = $this->model->save($data_entity);
                    $result2 = $this->model->wechat()->save(['connect'=>json_encode($data_connect)]);
                    if (($result1 !== false) and ($result2 !== false)) {
                        $this->success();
                    } else {
                        $this->error($this->model->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 配置编辑
     */
    public function edit($ids = NULL)
    {
        //测试数据
        //$data = ['app_id'=>'33133','secret'=>'dfadf','token'=>'EFFDdf','aes_key'=>''];
        //$data = json_encode($data);

        $row = $this->model->get($ids);
        $wechat = $row->wechat()->find();

        if (!$row)
            $this->error(__('No Results were found'));
        if ($this->request->isPost()) {
            $data_entity = $this->request->post("row/a");
            $data_connect = $this->request->post("connect/a");
            if ($data_entity) {
                foreach ($data_entity as $k => &$v) {
                    $v = is_array($v) ? implode(',', $v) : $v;
                }
                foreach ($data_connect as $k => &$v) {
                    $v = is_array($v) ? implode(',', $v) : $v;
                }
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = basename(str_replace('\\', '/', get_class($this->model)));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : true) : $this->modelValidate;
                        $row->validate($validate);
                    }
                    $result1 = $row->save($data_entity);
                    $result2 = $wechat->save(['connect'=>json_encode($data_connect)]);
                    if (($result1 !== false) and ($result2 !== false)) {
                        $this->success();
                    } else {
                        $this->error($row->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        $this->view->assign("connect", (array)json_decode($wechat->connect, true));
        return $this->view->fetch();
    }

    /**
     * 同步
     */
    public function sync($ids = NULL)
    {
        $app = new Application(get_addon_config('wechat'));
        try {
            $hasError = false;
            $menu = json_decode($this->wechatcfg->value, TRUE);
            foreach ($menu as $k => $v) {
                if (isset($v['sub_button'])) {
                    foreach ($v['sub_button'] as $m => $n) {
                        if ($n['type'] == 'click' && isset($n['key']) && !$n['key']) {
                            $hasError = true;
                            break 2;
                        }
                    }
                } else if ($v['type'] == 'click' && isset($v['key']) && !$v['key']) {
                    $hasError = true;
                    break;
                }
            }
            if (!$hasError) {
                try {
                    $ret = $app->menu->add($menu);
                } catch (\EasyWeChat\Core\Exceptions\HttpException $e) {
                    $this->error($e->getMessage());
                }
                if ($ret->errcode == 0) {
                    $this->success();
                } else {
                    $this->error($ret->errmsg);
                }
            } else {
                $this->error(__('Invalid parameters'));
            }
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
