<?php
namespace app\admin\controller\cms;

use app\admin\model\cms\Collect as CollectModel;
use app\admin\model\cms\Channel;
use app\admin\model\cms\Modelx;
use app\admin\model\cms\Archives;
use think\Db;
use QL\QueryList;
use GuzzleHttp\Exception\RequestException;

/**
 * 采集管理
 * @author Rock
 * @version 1.0.1
 * @base on fastAdmin cms插件
 * @icon fa fa-circle-o
 */
class Collect extends CmsBackend
{
    //Collect模型对象
    protected $model = null;
    protected $noNeedRight = ['get_channel_fields', 'check_element_available'];
    protected $searchFields = 'id,title';

    //数据库
    protected $base_table = 'cms_archives';
    protected $addon_table = 'cms_addonnews';

    // 采集参数
    protected $urls = null; // 当前采集url
    protected $list_range = null;
    protected $list_rules = [];
    protected $content_range = null;
    protected $content_rules = [];
    protected $channel_id = '';
    protected $model_id = '';
    protected $base_url = '';

    // 字段规范
    protected $base_fields = ['channel_id','model_id','title','link','hits','updatetime','image'];
    protected $addon_fields = ['id','content','author','image_url','attachment_name','attachment_url','title_description'];

    // 采集控制
    protected $continue = true; // 是否继续采集


    public function _initialize()
    {
        parent::_initialize();
        // 模型初始化
        $this->model = new CollectModel();
    }

    public function collect_start()
    {
        if ($this->request->isPost()) {

            $params = $this->request->post();
            // 检查访问授权
            if (isset($params['ids'])) {
                $ids = (int)$params['ids'];
                if (!$this->check_channel_auth($ids)) {
                    $this->error(__('You have no permission'));
                }
            }else{
                $this->error(__('Parameter %s can not be empty', 'id'));
            }

            $row = Channel::get($ids);
            if (!$row) $this->error(__('No Results were found'));
            $this->channel_id = $row['id'];
            $this->model_id = $row['model_id'];

            try {
                // 是否采用模型验证
                if ($this->modelValidate) {
                    $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                    $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                    $row->validate($validate);
                }

                $count = $this->collectData($params);
                if ($count !== false) {
                    $this->success("共采集数据".$count."条");
                } else {
                    $this->error($row->getError());
                }
            } catch (\think\exception\PDOException $e) {
                $this->error($e->getMessage());
            } catch (\think\Exception $e) {
                $this->error($e->getMessage());
            }
        }
    }

    private function collectData($params)
    {
        if (!is_array($params)) return false;
        if (!isset($params['ids'])) return false;
        if (!isset($params['base_url'])) return false;
        if (!isset($params['urls'])) return false;
        if (!isset($params['list_range'])) return false;
        if (!isset($params['list_rules'])) return false;
        if (!isset($params['content_range'])) return false;
        if (!isset($params['content_rules'])) return false;
        $ids = $params['ids'];
        $this->base_url = $params['base_url'];
        $this->urls = $params['urls'];
        $this->list_range = $params['list_range'];
        $this->list_rules = multi_to_array1($params['list_rules']);
        $this->content_range = $params['content_range'];
        $this->content_rules = multi_to_array1($params['content_rules']);

        $count = 0;
        $page = 0;
        // 分页采集
        do {
            if ($page == 0) {
                $index_file = "index" . '.htm';
            } else {
                $index_file = "index" . $page . '.htm';
            }
            $url = $this->urls . $index_file;
            $currentPage_count =  $this->currentPage_collect($url);
            $count += $currentPage_count;
            $page++;
        } while ($this->continue and $currentPage_count <> 0);
        // 更新栏目文章数
        $item = channel::where(['id'=>$ids])->find()->toArray();
        $article_count = $item['items'];
        $article_count += $count;
        channel::where(['id'=>$ids])->update(['items'=>$article_count]);

        return $count;

    }

    private function currentPage_collect($url)
    {
        try {
            //采集数据，形成源数据
            $source = QueryList::get($url)
                ->rules($this->list_rules)
                ->range($this->list_range)
                ->queryData(function ($item) {
                    $test =
                        QueryList::get($this->urls . $item['link'])
                            ->rules($this->content_rules)
                            ->range('')->queryData();
                    $item['content'] = $test;
                    return $item;
                });
            $suc = true;
        } catch (RequestException $e){
            $suc = false;
        }
        if ($suc){
            //格式化数据
            $base = ['channel_id'=>$this->channel_id, 'model_id'=>$this->model_id];
            $data = $this->formatData($source, $base);

            //存储数据，并返回数量等信息
            $count = $this->storage_data($data);
            return $count;
        } else {
            return 0;
        }
    }

    /**
     * 格式化采集到的数据，转化成标准的多维数组形式，遇到重复数据就退出循环
     * @param $source 源数据
     * @param $base 源数据的公共数据
     * @return array|bool 格式化后的数据
     */
    private function formatData($source, $base)
    {
        if (!is_array($source)) return false;
        $data = [];
        foreach ($source as $key => $item){
            $data[$key] = $base;
            if (isset($item['caption'])) $data[$key]['caption'] = trim($item['caption']);
            if (isset($item['link'])) $data[$key]['link'] = $this->urls . $item['link'];
            if (isset($item['updatetime'])) $data[$key]['updatetime'] = $this->format_time($item['updatetime']);
            if (is_array($item['content'])){
                foreach ($item['content'] as $item1){
                    if (isset($item1['content'])) {
                        $item1['content'] = formatUrl($item1['content'], $this->base_url);
                        $data[$key]['content'] = $item1['content'];
                    }
                    if (isset($item1['title'])) $data[$key]['title'] = trim($item1['title']);
                    if (isset($item1['title_description'])) $data[$key]['title_description'] = trim($item1['title_description']);
                    if (isset($item1['image_url'])) $data[$key]['image_url'][] = $this->base_url . '/' . substr($item1['image_url'], 6);
                }
            }
            // 取首张图片
            if (isset($data[$key]['image_url'])) $data[$key]['image'] = $data[$key]['image_url'][0];
            // 图片数组转化成字符串
            if (isset($data[$key]['image_url']) and is_array($data[$key]['image_url'])) $data[$key]['image_url'] = implode(',',$data[$key]['image_url']);
            // 数据查重
            $item_existed = $this->item_existed($data[$key]);
            if ($item_existed === 'updated'){
                $data[$key]['status'] = "update";
            } elseif ($item_existed === null){
                $data[$key]['status'] = 'insert';
            } else {
                $data[$key]['status'] = 'ignore';
                $this->continue = false;
                break; // 遇到重复数据则退出循环
            }
        }
        return $data;
    }

    /**
     * 采集数据的存储
     * @param array $data 格式化后的数据
     * @return int $count 成功存储记录数
     */
    private function storage_data($data)
    {
        $count = 0;
        Db::startTrans();
        try{
            foreach ($data as $item){
                if ($item['status'] === "ignore") break;
                $base_data = [];
                foreach ($this->base_fields as $key){
                    if (array_key_exists($key, $item)) $base_data[$key] = $item[$key];
                }
                if ($item['status'] == 'update'){
                    $where['link'] = ['eq', $item['link']];
                    $id = Db::name($this->base_table)->where[$where]->update($base_data);
                } else {
                    $id = Db::name($this->base_table)->insertGetId($base_data);
                }

                $addon_data = [];
                $addon_data['id'] = $id;
                foreach ($this->addon_fields as $key){
                    if (array_key_exists($key, $item)) $addon_data[$key] = $item[$key];
                }
                if ($item['status'] == 'update'){
                    $where['id'] = ['eq', $id];
                    Db::name($this->addon_table)->where[$where]->update($addon_data);
                } else {
                    Db::name($this->addon_table)->insert($addon_data);
                }

                $count++;
            }

            Db::commit();
        } catch (\think\Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        return $count;

    }

    /**
     * 字符串转时间戳
     * @param $updatetime 字符串形式的日期
     * @return int 转化后的时间戳
     */
    private function format_time($updatetime)
    {
        $result = strtotime($updatetime);
        return $result;
    }

    private function item_existed($item)
    {
        if (!is_array($item)) return false;
        $where['link'] = ['eq', $item['link']];
        $result = Archives::where($where)->find();
        if ($result){
            if ($item['updatetime'] == $result['updatetime']){
                return true;
            } else {
                return 'updated';
            }
        }
        return $result;
    }
}
