<?php
/**
 * Created by Rock
 * Date: 2019-3-31
 * Time: 上午 11:51
 */
namespace app\admin\controller\cms;

use app\common\controller\Backend;
use app\admin\model\cms\Channel;
use app\admin\model\cms\ChannelAdmin;
use app\common\model\Entity;


class CmsBackend extends Backend
{
    //权限设定
    protected $isSuperAdmin = false; //是否为超管
    protected $isEntityAdmin = false; //是否为实体创建者
    //栏目设定
    protected $allChannelList = [];
    protected $channelList = [];
    protected $channelIds = []; //栏目Id列表
    protected $parentChannelIds = []; //父级栏目Id列表
    protected $disabledIds = [];
    //实体属性
    protected $currentEntityId = null; // 当前实体id

    public function _initialize()
    {
        parent::_initialize();

        //权限初始化
        $this->isSuperAdmin = $this->auth->isSuperAdmin(); //是否超级管理员
            //获取当前实体Id
        $params = $this->request->request();
        if (isset($params['currentEntityId'])){
            $this->currentEntityId = $params['currentEntityId'];
        } else if (isset($this->entityIdList)){
            $this->currentEntityId = key($this->entityIdList);
        }
        $this->isEntityAdmin = Entity::isEntityAdmin($this->currentEntityId, $this->auth->id); // 判断是否为实体创建者
        //允许的栏目
        $this->allChannelList = collection(Channel::where('entity_id', 'eq', $this->currentEntityId)->select())->toArray(); // 指定entity_id的全部栏目
        $this->channelIds = ($this->isSuperAdmin or $this->isEntityAdmin) ?
            Channel::where('entity_id', 'eq', $this->currentEntityId)->column('id') :
            ChannelAdmin::getAdminChannelIds();
        $this->parentChannelIds = Channel::where('id', 'in', $this->channelIds)->column('parent_id');

        //栏目列表初始化
        foreach ($this->allChannelList as $k => $v) {
            $state = ['opened' => true];
            if ($v['type'] != 'list') {
                $this->disabledIds[] = $v['id'];
            }
            if ($v['type'] == 'link') {
                $state['checkbox_disabled'] = true;
            }
            if (!$this->isSuperAdmin and !$this->isEntityAdmin) {
                if (($v['type'] != 'list' && !in_array($v['id'], $this->parentChannelIds)) || ($v['type'] == 'list' && !in_array($v['id'], $this->channelIds))) {
                    unset($this->allChannelList[$k]);
                    continue;
                }
            }
            $this->channelList[] = [
                'id'     => $v['id'],
                'parent' => $v['parent_id'] ? $v['parent_id'] : '#',
                'text'   => __($v['name']),
                'type'   => $v['type'],
                'state'  => $state
            ];
        }
    }

    /**
     * 栏目访问权限检查
     * @author Rock
     * @param $id channel Id
     * @return bool
     */
    public function check_channel_auth($id)
    {
        if (empty($id)) return false;
        if (empty($this->channelIds)) return false;
        if (in_array($id, $this->channelIds))
            return true;
        else
            return false;
    }

}