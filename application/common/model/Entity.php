<?php

namespace app\common\model;

use app\common\model\MoneyLog;
use app\admin\model\wechat\WechatBase;
use think\Model;

/**
 * 实体模型
 * Class Entity
 * @author Rock
 * @package app\admin\model
 */
class Entity extends Model
{

    // 表名
    protected $name = 'entity';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    // 属性配置
    protected $typeConfig = ['wechat'=>'公众号','cms'=>'微站','message'=>'消息','archives'=>'资料室','group'=>'团队'];
    protected $statusConfig = ['running'=>'已启用','closed'=>'已关闭','freezed'=>'已冻结', 'unauthorized'=>'未授权'];

    private $filter = [];

    // 关联模型
    public function wechat()
    {
        return $this->hasOne('app\admin\model\wechat\WechatBase', 'entity_id');
    }

    public function setFilter($where)
    {
        $this->filter = $where;
    }

    /*
    protected function base($query)
    {
        $query->where('id',1);
    }
    */
    protected function scopeId($query)
    {
        $query->where($this->filter);
    }
    protected function scopeType($query)
    {
        $query->where($this->filter);
    }

    public function getOriginData()
    {
        return $this->origin;
    }

    protected static function init()
    {

    }

    /**
     * 根据指定用户的资源列表及请求path，得到对应实体Ids的基本信息列表
     * @author Rock
     * @param $resourceList 指定用户的资源列表
     * @param $path 请求path
     * @return array $entityList 实体资源Id列表
     */
    public static function getEntityIdList($resourceList, $path)
    {
        if (empty($resourceList)) return [];
        $ids = [];
        if (isset($resourceList[$path]))
        {
            $ids = $resourceList[$path]['entity_id'];
            if ($ids == []) return [];
        } else {
            return [];
        }
        $where = [];
        $where['id'] = ['in', $ids];
        $result = self::where($where)->column('id,name');
        return $result;
    }

    /**
     * 判断是否为指定entity的管理员
     * @param $entity_id
     * @param $user_id
     * @return boolean 是/否
     */
    public static function isEntityAdmin($entity_id, $user_id)
    {
        if (empty($entity_id) or empty($user_id)) return false;
        $filter = [];
        $filter['id'] = ['eq', $entity_id];
        $filter['admin_id'] = ['eq', $user_id];
        $result = self::where($filter)->column('id');
        return $result ? true : false;
    }

    /**
     * 根据指定用户的资源列表及请求path，得到对应实体的基本信息列表
     * @author Rock
     * @param $resourceList 指定用户的资源列表
     * @param $path 请求path
     * @return array $entityList 实体资源列表
     */
    public function getEntityList($resourceList, $path)
    {
        if (empty($resourceList)) return [];
        $ids = [];
        if (isset($resourceList[$path]))
        {
            $ids = $resourceList[$path]['entity_id'];
            if ($ids == []) return [];
        } else {
            return [];
        }
        $where = [];
        $where['id'] = ['in', $ids];
        $result = $this->where($where)->field('id,pid,name,user_id,createtime,updatetime,icon,type,status')->select();
        return collection($result)->toArray();
    }

    /**
     * 获取过滤后的实体对应的规则资源列表
     * @author Rock
     * @param array $groupAccessList 实体对应的规则资源列表
     * @param int $pid 规则资源的上级资源ID
     * @param string $flag 规则资源的识别标记，如：wechat_entity
     * @return array 指定$pid及$flag下过滤后的实体对应的规则资源列表
     */
    public function getGroupAccessList($groupAccessList, $pid=0, $flag='' )
    {
        $list = [];
        if (!is_array($groupAccessList)) return [];
        foreach ($groupAccessList as $key => $g)
        {
            foreach ($g as $key1 => $a)
            {
                if (($a['pid'] !== $pid) or ($a['flag'] !== $flag))
                {
                    unset($g[$key1]);
                }
            }
            if (count($g) <> 0) $list[$key] = $g;
        }
        return $list;
    }

    public function getGenderList()
    {
        return ['1' => __('Male'), '0' => __('Female')];
    }

    public function getStatusList()
    {
        return ['normal' => __('Normal'), 'hidden' => __('Hidden')];
    }

    public function getPrevtimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['prevtime'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getLogintimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['logintime'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getJointimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['jointime'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setPrevtimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    protected function setLogintimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    protected function setJointimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    public function group()
    {
        return $this->belongsTo('UserGroup', 'group_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

}
