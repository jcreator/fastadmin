<?php
/**
 * Created by Rock.
 * User: Administrator
 * Date: 2019-3-14
 * Time: 下午 16:39
 */
namespace app\common\library;

use addons\third\model\ThirdUser;
use app\admin\model\User;
use fast\Random;
use app\common\library\Auth;
use think\Cache;
use think\Db;

class ThirdSession
{
    /**
     * 默认配置
     * @var array
     */
    protected static $options = [
        'expire'     => 3600
    ];

    protected static $_token = ''; //缓存键值
    protected static $_user_field = 'username, email, mobile';

    /**
     * 创建新缓存键值（token），根据传入的openid查找对应的userInfo信息，
     * 将openid、sessionKey，userInfo一并存入缓存，
     * @param array $data 用户初始化数据
     * @param string $platform 平台标识，缺省为wxapp
     * @param int $expire 缓存过期时间
     * @return array['ret'=> true, 'msg'=>['token'=>session_key, 'value'=>userInfo]]
     */
    public static function init( $data = [], $platform = 'wxapp', $expire = 0 )
    {
        if (!is_array($data) || $platform !== 'wxapp') return FALSE;

        if (isset($data['openid'])){
            $ret1 = ThirdUser::where('openid', $data['openid'])->field('openid, user_id')->find()->toArray();
            if (!is_array($ret1)){
                Db::startTrans();
                try{
                    // TODO: 在Third表查无openid记录时，openid新增user表记录、新增ThirdUser表记录
                    Db::commit();
                } catch (\Exception $e) {
                    Db::rollback();
                }
            }
            $ret2 = User::get(['id'=>$ret1['user_id']])->toArray();
            //field('username, email, mobile')->find()->toArray();
            if (!is_array($ret2)){
                // TODO: 在User表查无user_id记录时，新增user表记录
            }
            $ret2_sec = ['username'=> $ret2['username'], 'email'=> $ret2['email'], 'mobile'=> $ret2['mobile']];
            $data = array_merge($ret1, $ret2_sec, $data);
            $result['token'] = self::newSession($data, $expire);
            unset($data['openid']);
            unset($data['session_key']);
            $result['code'] = 1;
            $result['userInfo']  = $data;
            return $result;
        }
        return FALSE;
    }


    /**
     * 创建新缓存
     * @param array $data 缓存值
     * @param null $expire 缓存时间
     * @return string 缓存键值
     */
    public static function newSession( $data = [], $expire = null )
    {
        $expireTime = !is_null($expire) ? (int)$expire : self::$options['expire'];
        self::$_token = Random::uuid();
        Cache::set(self::$_token, $data, $expireTime);
        return self::$_token;
    }


    /**
     * 获取Token内的信息
     * @param   string $token
     * @return  array
     */
    public function get($token)
    {
        $encryptedToken = $this->getEncryptedToken($token);
        $data = $this->handler->where('token', $encryptedToken)->find();
        if ($data) {
            if (!$data['expiretime'] || $data['expiretime'] > time()) {
                //返回未加密的token给客户端使用
                $data['token'] = $token;
                //返回剩余有效时间
                $data['expires_in'] = $this->getExpiredIn($data['expiretime']);
                return $data;
            } else {
                self::delete($token);
            }
        }
        return [];
    }

    /**
     * 判断Token是否可用
     * @param   string $token Token
     * @param   int $user_id 会员ID
     * @return  boolean
     */
    public function check($token, $user_id)
    {
        $data = $this->get($token);
        return $data && $data['user_id'] == $user_id ? true : false;
    }

    /**
     * 删除Token
     * @param   string $token
     * @return  boolean
     */
    public function delete($token)
    {
        $this->handler->where('token', $this->getEncryptedToken($token))->delete();
        return true;
    }

    /**
     * 删除指定用户的所有Token
     * @param   int $user_id
     * @return  boolean
     */
    public function clear($user_id)
    {
        $this->handler->where('user_id', $user_id)->delete();
        return true;
    }

}